#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

enum bmp_load_status{
	BMP_LOAD_OK = 0,
	BMP_LOAD_EOF,
	BMP_LOAD_FSEEK_ERR,
	BMP_LOAD_FAILED,
	BMP_LOAD_HEADER_OK,
	BMP_LOAD_HEADER_EOF,
	BMP_LOAD_HEADER_FAILED,
	BMP_LOAD_IMAGE_CREATE_FAILED
};


enum bmp_save_status{
	BMP_SAVE_OK = 0,
	BMP_SAVE_FAILED,
	BMP_SAVE_HEADER_FAILED
};

char* get_bmp_load_msg(enum bmp_load_status status);

char* get_bmp_save_msg(enum bmp_save_status status);

enum bmp_load_status from_bmp(FILE* in_fd, struct image* img);

enum bmp_save_status to_bmp(FILE* out_fd, struct image img);

#endif
