#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <errno.h>
#include <stdio.h>


enum open_status{
	OPEN_OK = 0,
	OPEN_NO_FILE = ENOENT,
	OPEN_UNKNOWN = 255
};


enum close_status{
	CLOSE_OK = 0,
	CLOSE_UNKNOWN = 255
};

char* get_open_msg(enum open_status status);
char* get_close_msg(enum close_status status);

enum open_status file_open(char const* name, FILE** fd, char const* mode);

enum close_status file_close(FILE* fd);

#endif
