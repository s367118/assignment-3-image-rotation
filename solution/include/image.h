#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stdio.h>

enum image_create_status{
	IMAGE_CREATE_OK = 0,
	IMAGE_CREATE_FAILED
};

char* get_image_create_msg(enum image_create_status status);


struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};

enum image_create_status image_create(struct image* image, uint64_t width, uint64_t height);

void image_free(struct image* image);

void print_image_pixels(struct image img);

#endif
