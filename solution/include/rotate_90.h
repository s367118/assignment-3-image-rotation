#ifndef ROTATE_90_H
#define ROTATE_90_H

#include "image.h"

enum rotate_90_status{
	ROTATE_OK = 0,
	ROTATE_IMAGE_CREATE_FAILED
};
char* get_rotate_90_msg(enum rotate_90_status status);

enum rotate_90_status rotate_90(struct image* img);

#endif
