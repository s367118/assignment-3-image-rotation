#include "bmp.h"

#include "image.h"

#include <stdint.h>
#include <stdio.h>




char* bmp_load_msg[] = {
	[BMP_LOAD_OK] = "BMP load ok",
	[BMP_LOAD_EOF] = "BMP load failed: reach EOF",
	[BMP_LOAD_FSEEK_ERR] = "BMP load failed: fseek error",
	[BMP_LOAD_FAILED] = "BMP load failed",
	[BMP_LOAD_HEADER_OK] = "BMP header load ok",
	[BMP_LOAD_HEADER_EOF] = "BMP header load failed: reach EOF",
	[BMP_LOAD_HEADER_FAILED] = "BMP header load failed",
	[BMP_LOAD_IMAGE_CREATE_FAILED] = "BMP load failed: can not create image"
};

char* get_bmp_load_msg(enum bmp_load_status status){
	return bmp_load_msg[status];
}

char* bmp_save_msg[] = {
	[BMP_SAVE_OK] = "BMP save ok",
	[BMP_SAVE_FAILED] = "BMP save failed",
	[BMP_SAVE_HEADER_FAILED] = "BMP header save failed"
};

char* get_bmp_save_msg(enum bmp_save_status status){
	return bmp_save_msg[status];
}

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
} last_bmp_header;

static uint8_t count_padding(uint64_t width){
	uint64_t length = sizeof(struct pixel) * width;
	uint8_t padding = (uint8_t)((4 - length%4) % 4);
	return padding;
}

static enum bmp_load_status bmp_header_read(FILE* fd, struct bmp_header* header){
	size_t header_size = sizeof(struct bmp_header);
	size_t read_header_res = fread(header, header_size, 1, fd);
    if(read_header_res != 1){
        if (feof(fd)){
            return BMP_LOAD_HEADER_EOF;
        }
        if (ferror(fd)){
        	return BMP_LOAD_HEADER_FAILED;
        }
    }
    return BMP_LOAD_HEADER_OK;
}

enum bmp_load_status from_bmp(FILE* in_fd, struct image* img){
	enum bmp_load_status read_header_status = bmp_header_read(in_fd, &last_bmp_header);
	if (read_header_status != BMP_LOAD_HEADER_OK){
		return read_header_status;
	}
	
	enum image_create_status img_create_status = image_create(img, last_bmp_header.biWidth, last_bmp_header.biHeight);
	if (img_create_status != IMAGE_CREATE_OK){
		return BMP_LOAD_IMAGE_CREATE_FAILED;
	}
	
	if (fseek(in_fd, (long)last_bmp_header.bOffBits, SEEK_SET) != 0){
		return BMP_LOAD_FSEEK_ERR;
	}
	
	size_t read_pixels_total = 0;
	uint64_t width = img->width;
	uint64_t height = img->height;
	uint8_t padding = count_padding(width);

	for(uint64_t row = 0; row < height; row++){
		size_t read_pixels = fread(&((img->data)[read_pixels_total]), sizeof(struct pixel), width, in_fd);
    	if(read_pixels != width){
		    if (feof(in_fd)){
		        return BMP_LOAD_EOF;
		    }
		    if (ferror(in_fd)){
		    	return BMP_LOAD_FAILED;
		    }
    	}
    	read_pixels_total += read_pixels;
    	
		if (fseek(in_fd, padding, SEEK_CUR) != 0){
			return BMP_LOAD_FSEEK_ERR;
		}
	}
	
	return BMP_LOAD_OK;
}

enum bmp_save_status to_bmp(FILE* out_fd, struct image img){
	uint64_t width = img.width;
	uint64_t height = img.height;
	
	last_bmp_header.biWidth = width;
	last_bmp_header.biHeight = height;
	size_t write_header = fwrite(&last_bmp_header, sizeof(struct bmp_header), 1, out_fd);
	if (write_header != 1){
		return BMP_SAVE_HEADER_FAILED;
	}
	
	if (fseek(out_fd, (long)last_bmp_header.bOffBits, SEEK_SET) != 0){
		return BMP_SAVE_FAILED;
	}
	
	size_t write_pixels_total = 0;
	for(uint64_t row = 0; row < height; row++){
		size_t write_pixels = fwrite(&(img.data)[write_pixels_total], sizeof(struct pixel), width, out_fd);
    	if(write_pixels != width){
		    return BMP_SAVE_FAILED;
    	}
    	write_pixels_total += write_pixels;
    	
    	char place_holder = 'a';
    	for(int i=0; i < count_padding(width); i++)
        {
            int write_byte = putc(place_holder, out_fd);
            if (write_byte == EOF){
            	return BMP_SAVE_FAILED;
            }
        }
	}
	return BMP_SAVE_OK;
}

