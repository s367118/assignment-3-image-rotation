#include "file_manager.h"

#include <errno.h>
#include <stdio.h>




char* open_msg[] = {
	[OPEN_OK] = "Open file",
	[OPEN_NO_FILE] = "No such file",
	[OPEN_UNKNOWN] = "Something went wrong: open file"
};

char* get_open_msg(enum open_status status){
	char* msg = open_msg[status];
	if (msg == NULL){
		return open_msg[OPEN_UNKNOWN];
	}
	return msg;
}

char* close_msg[] = {
	[CLOSE_OK] = "Close file",
	[CLOSE_UNKNOWN] = "Something went wrong: close file"
};

char* get_close_msg(enum close_status status){
	return close_msg[status];
}

enum open_status file_open(char const* name, FILE** fd, char const* mode){
	*fd = fopen(name, mode);
	if(*fd == NULL)
    {
    	return errno;
    }
    return OPEN_OK;
}

enum close_status file_close(FILE* fd){
	if(fclose(fd) == EOF)
    {
    	return CLOSE_UNKNOWN;
    }
    return CLOSE_OK;
}

