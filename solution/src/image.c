#include "image.h"

#include <inttypes.h>
#include <stdlib.h>




char* image_create_msg[] = {
	[IMAGE_CREATE_OK] = "Create image",
	[IMAGE_CREATE_FAILED] = "Memory alloc for image failed"
};

char* get_image_create_msg(enum image_create_status status){
	return image_create_msg[status];
}

enum image_create_status image_create(struct image* image, uint64_t width, uint64_t height){
	image->width = width;
	image->height = height;
	struct pixel* pixels = malloc( sizeof(struct pixel) * width * height );
	if (pixels == NULL){
		return IMAGE_CREATE_FAILED;
	}
	image->data = pixels;
	return IMAGE_CREATE_OK;
}

void image_free(struct image* image){
	if (image->data != NULL){
		free(image->data);
	}
	image->data = NULL;
}

void print_image_pixels(struct image img){
	uint64_t pixels = 0;
	for(uint64_t height = 0; height < img.height; height++){
		for(uint64_t width = 0; width < img.width; width++){
			printf("[""%"PRIu8",""%"PRIu8", ""%"PRIu8"]  ",  (img.data[pixels]).r, (img.data[pixels]).g, (img.data[pixels]).b);
			pixels++;
		}
		printf("\n");
	}
}
