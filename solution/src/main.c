#include "bmp.h"
#include "file_manager.h"
#include "image.h"
#include "rotate_90.h"

#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

//declare files and imgs as global vars
//as they should be closed even if error raised
FILE* in_fd = NULL;
FILE* out_fd = NULL;
struct image img = {.data = NULL};

void exit_error(char const* msg);

void check_args_count(int argc){
	int expect_args_count = 4;
	if (argc < expect_args_count){
		exit_error("Need more args. Use template:\n./image-transformer <source-image> <transformed-image> <angle>");
	}
}

void check_angle(long angle){
	if (angle % 90 != 0){
		exit_error("Angle arg should be multiple of 90");
	}
}

void try_open(char const* file_name, FILE** fd, char const* mode){
	enum open_status opened_status = file_open(file_name, fd, mode);
	if (opened_status != OPEN_OK){
		exit_error(get_open_msg(opened_status));
	}
}

void try_close(FILE* fd){
	if (fd == NULL){return;} 
	enum close_status closed_status = file_close(fd);
	if (closed_status != CLOSE_OK){
		fd = NULL;	//work once
		exit_error(get_close_msg(closed_status));
	}
}

void try_load_bmp(FILE* in_file, struct image* image){
	enum bmp_load_status bmp_load = from_bmp(in_file, image);
	if (bmp_load != BMP_LOAD_OK){
		exit_error(get_bmp_load_msg(bmp_load));
	}
}

void try_save_bmp(FILE* out_file, struct image image){
	enum bmp_save_status bmp_save = to_bmp(out_file, image);
	if (bmp_save != BMP_SAVE_OK){
		exit_error(get_bmp_save_msg(bmp_save));
	}
}

void try_rotate(struct image* image, int angle){
	int rotate_count = (angle%360)/90;
	if (rotate_count < 0){
		rotate_count += 4;
	}
	for(int i = 0; i < rotate_count; i++){
		enum rotate_90_status rotate_status = rotate_90(image);
		if (rotate_status != ROTATE_OK){
			exit_error(get_rotate_90_msg(rotate_status));
		}
	}
}

void exit_app(int code){
	try_close(in_fd);
	try_close(out_fd);
	image_free(&img);
	exit(code);
}

void exit_error(char const* msg){
	fprintf(stderr, "%s \n", msg);
	
	if (in_fd != NULL){
		file_close(in_fd);
	}
	if (out_fd != NULL){
		file_close(out_fd);
	}
	image_free(&img);
	exit(-1);
}


int main(int argc, char* argv[]){

	check_args_count(argc);

	char* in_name = argv[1];
	char* out_name = argv[2];
	long angle = strtol(argv[3], NULL, 10);
	if(errno == ERANGE){
		exit_error("Exception in reading angle value");
	}
	
	check_angle(angle);

	try_open(in_name, &in_fd, "rb");
	
	try_load_bmp(in_fd, &img);
	
	try_rotate(&img, (int)angle);
	
	try_open(out_name, &out_fd, "wb");

	try_save_bmp(out_fd, img);
	
	exit_app(0);
}
