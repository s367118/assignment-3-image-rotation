#include "rotate_90.h"

#include "image.h"

#include <inttypes.h>
#include <stdlib.h>





char* rotate_90_msg[] = {
	[ROTATE_OK] = "Rotate image",
	[ROTATE_IMAGE_CREATE_FAILED] = "Memory alloc for image failed"
};

char* get_rotate_90_msg(enum rotate_90_status status){
	return rotate_90_msg[status];
}

enum rotate_90_status rotate_90(struct image* img){
	uint64_t width = img->width;
	uint64_t height = img->height;
	struct image new;
	enum image_create_status img_create_status = image_create(&new, width, height);
	if (img_create_status != IMAGE_CREATE_OK){
		return ROTATE_IMAGE_CREATE_FAILED;
	}
	
	
	size_t pixel = 0;
	for(uint64_t column = width - 1; column != 0-1; column--){
		for(uint64_t row = 0; row < height; row++){
			new.data[pixel] = img->data[row*width + column];
			pixel++;
		}
	}
		
	image_free(img);
	img->height = width;
	img->width = height;
	img->data = new.data;
	
	
	return ROTATE_OK;
}
